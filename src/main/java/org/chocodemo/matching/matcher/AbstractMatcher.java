package org.chocodemo.matching.matcher;

import java.util.ArrayList;
import java.util.List;

import org.chocodemo.matching.matchable.Matchable;
import org.chocosolver.solver.variables.IntVar;

public abstract class AbstractMatcher implements IMatcher {

	protected int[] quantityArray(List<Matchable> otherSide) {
		return otherSide.stream().mapToInt(matchable -> matchable.getQuantity()).toArray();
	}
	
	protected int[] priceArray(List<Matchable> otherSide) {
		return otherSide.stream().mapToInt(matchable -> matchable.getPrice()).toArray();
	}
	
	protected List<Matchable> createResultList(List<Matchable> matchableList, IntVar[] solution) {
		List<Matchable> result = new ArrayList<>();
		for (int i = 0; i < matchableList.size(); i++) {
			if (solution[i].getValue() == 1) {
				result.add(matchableList.get(i));
			}
		}

		return result;
	}
	
	protected int[] vectorProduct(int[] quantites, int[] prices) {
		int[] result = new int[quantites.length];
		for (int i = 0; i < quantites.length; i++){
			result[i] = quantites[i] * prices[i];
		}
		return result;
	}
	
}
