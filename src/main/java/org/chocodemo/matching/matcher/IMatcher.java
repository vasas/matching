package org.chocodemo.matching.matcher;

import java.util.List;

import org.chocodemo.matching.matchable.Matchable;


public interface IMatcher {
	
	MatchResult match(Matchable matchable, List<Matchable> ownSide, List<Matchable> otherSide);
	
}
