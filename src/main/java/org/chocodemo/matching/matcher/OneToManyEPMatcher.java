package org.chocodemo.matching.matcher;

import static java.util.Collections.singletonList;
import static org.chocosolver.solver.constraints.IntConstraintFactory.knapsack;
import static org.chocosolver.solver.variables.VariableFactory.boundedArray;
import static org.chocosolver.solver.variables.VariableFactory.fixed;

import java.util.List;

import org.chocodemo.matching.matchable.Matchable;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.constraints.Constraint;
import org.chocosolver.solver.variables.IntVar;

public class OneToManyEPMatcher extends AbstractMatcher {

	@Override
	public MatchResult match(Matchable matchable, List<Matchable> ownSide, List<Matchable> otherSide) {

		Solver solver = new Solver();

		IntVar[] solution = boundedArray("solution", otherSide.size(), 0, 1, solver);
		IntVar targetQuantity = fixed(matchable.getQuantity(), solver);
		int[] quantities = quantityArray(otherSide);

		Constraint knapsack = knapsack(solution, targetQuantity, targetQuantity, quantities, quantities);
		solver.post(knapsack);
		boolean found = solver.findSolution();

		if (!found) {
			return MatchResult.EMPTY;
		}

		List<Matchable> resultOtherSide = createResultList(otherSide, solution);

		return new MatchResult(singletonList(matchable), resultOtherSide);

	}

}
