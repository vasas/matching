package org.chocodemo.matching.matcher;

import java.util.Collections;
import java.util.List;

import org.chocodemo.matching.matchable.Matchable;

public class MatchResult {

	public static final MatchResult EMPTY = new MatchResult(Collections.<Matchable> emptyList(),
			Collections.<Matchable> emptyList());

	private List<Matchable> otherSide;

	private List<Matchable> ownSide;

	public MatchResult(List<Matchable> ownSide, List<Matchable> otherSide) {
		this.otherSide = otherSide;
		this.ownSide = ownSide;
	}

	public List<Matchable> getOtherSide() {
		return Collections.unmodifiableList(otherSide);
	}

	public List<Matchable> getOwnSide() {
		return Collections.unmodifiableList(ownSide);
	}

	@Override
	public String toString() {
		return "MatchResult [otherSide=" + otherSide + ", ownSide=" + ownSide + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((otherSide == null) ? 0 : otherSide.hashCode());
		result = prime * result + ((ownSide == null) ? 0 : ownSide.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MatchResult other = (MatchResult) obj;
		if (otherSide == null) {
			if (other.otherSide != null)
				return false;
		} else if (!otherSide.equals(other.otherSide))
			return false;
		if (ownSide == null) {
			if (other.ownSide != null)
				return false;
		} else if (!ownSide.equals(other.ownSide))
			return false;
		return true;
	}
	
}
