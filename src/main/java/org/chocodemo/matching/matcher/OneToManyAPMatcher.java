package org.chocodemo.matching.matcher;

import static java.util.Collections.singletonList;
import static org.chocosolver.solver.constraints.IntConstraintFactory.scalar;
import static org.chocosolver.solver.variables.VariableFactory.fixed;

import java.util.List;

import org.chocodemo.matching.matchable.Matchable;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.constraints.Constraint;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.VariableFactory;

public class OneToManyAPMatcher extends AbstractMatcher {

	@Override
	public MatchResult match(Matchable matchable, List<Matchable> ownSide, List<Matchable> otherSide) {

		Solver solver = new Solver();
		
		IntVar[] solution = VariableFactory.boundedArray("solution", otherSide.size(), 0, 1, solver);
		
		IntVar quantity = fixed(matchable.getQuantity(), solver);
		IntVar priceTimesQuantity = fixed(matchable.getPrice() * matchable.getQuantity(), solver);
		
		int[] quantites = quantityArray(otherSide);
		int[] prices = priceArray(otherSide);
		int[] priceQuantityScalar = vectorProduct(quantites, prices);
		
		Constraint quantityConstraint = scalar(solution, quantites, quantity);
		Constraint priceConstraint = scalar(solution, priceQuantityScalar, priceTimesQuantity);
		
		solver.post(quantityConstraint);
		solver.post(priceConstraint);
		
		boolean found = solver.findSolution();
		
		if (!found) {
			return MatchResult.EMPTY;
		}

		List<Matchable> resultOtherSide = createResultList(otherSide, solution);

		return new MatchResult(singletonList(matchable), resultOtherSide);
	}
	
}
