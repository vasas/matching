package org.chocodemo.matching.matcher;

import static org.chocosolver.solver.constraints.IntConstraintFactory.arithm;
import static org.chocosolver.solver.constraints.IntConstraintFactory.scalar;
import static org.chocosolver.solver.variables.VariableFactory.MAX_INT_BOUND;
import static org.chocosolver.solver.variables.VariableFactory.boundedArray;
import static org.chocosolver.solver.variables.VariableFactory.integer;

import java.util.List;

import org.chocodemo.matching.matchable.Matchable;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.constraints.Constraint;
import org.chocosolver.solver.variables.IntVar;

public class ManyToManyEPMatcher extends AbstractMatcher {

	@Override
	public MatchResult match(Matchable matchable, List<Matchable> ownSide, List<Matchable> otherSide) {

		Solver solver = new Solver();

		IntVar[] ownSideSolution = boundedArray("ownSideSolution", ownSide.size(), 0, 1, solver);
		IntVar[] otherSideSolution = boundedArray("otherSideSolution", otherSide.size(), 0, 1, solver);
		
		IntVar ownSideSumProduct = integer("ownSideSumProduct", 0, MAX_INT_BOUND, solver);
		IntVar otherSideSumProduct = integer("otherSideSumProduct", 0, MAX_INT_BOUND, solver);
		
		int[] ownSideQuantities = quantityArray(ownSide);
		int[] otherSideQuantities = quantityArray(otherSide);
		
		Constraint ownSideSumProductConstraint = scalar(ownSideSolution, ownSideQuantities, ownSideSumProduct);
		Constraint otherSideSumProductConstraint = scalar(otherSideSolution, otherSideQuantities, otherSideSumProduct);
		Constraint equalConstraint = arithm(ownSideSumProduct, "=", otherSideSumProduct);
		
		// The input parameter must be in the match result
		Constraint elementConstraint = arithm(ownSideSolution[ownSide.indexOf(matchable)], "=", 1);
		
		solver.post(ownSideSumProductConstraint);
		solver.post(otherSideSumProductConstraint);
		solver.post(equalConstraint);
		solver.post(elementConstraint);
		
		boolean found = solver.findSolution();
		if (!found){
			return MatchResult.EMPTY;
		}
		
		return new MatchResult(createResultList(ownSide, ownSideSolution), createResultList(otherSide, otherSideSolution));
	}

}
