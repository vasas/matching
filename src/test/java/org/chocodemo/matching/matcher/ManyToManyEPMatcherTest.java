package org.chocodemo.matching.matcher;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertEquals;

import org.chocodemo.matching.matchable.Matchable;
import org.junit.Before;
import org.junit.Test;

public class ManyToManyEPMatcherTest {

	private ManyToManyEPMatcher matcher;

	@Before
	public void setUp() {
		matcher = new ManyToManyEPMatcher();
	}

	@Test
	public void test1() {
		Matchable ex1 = new Matchable("ex1", 6, 10);
		Matchable ex2 = new Matchable("ex2", 4, 10);

		Matchable exEx1 = new Matchable("exEx1", 1, 10);
		Matchable exEx2 = new Matchable("exEx2", 2, 10);
		Matchable exEx3 = new Matchable("exEx3", 3, 10);
		Matchable exEx4 = new Matchable("exEx4", 5, 10);
		Matchable exEx5 = new Matchable("exEx5", 5, 10);

		MatchResult result = matcher.match(ex2, asList(ex1, ex2), asList(exEx1, exEx2, exEx3, exEx4, exEx5));
		assertEquals(asList("exEx1", "exEx3"),
				result.getOtherSide().stream().map(m -> m.getId()).collect(toList()));
		assertEquals(asList("ex2"),
				result.getOwnSide().stream().map(m -> m.getId()).collect(toList()));

	}
	
	@Test
	public void test2() {
		Matchable ex1 = new Matchable("ex1", 6, 10);
		Matchable ex2 = new Matchable("ex2", 4, 10);
		
		Matchable exEx3 = new Matchable("exEx3", 3, 10);
		Matchable exEx4 = new Matchable("exEx4", 5, 10);
		Matchable exEx5 = new Matchable("exEx5", 5, 10);
		
		MatchResult result = matcher.match(ex1, asList(ex1, ex2), asList(exEx3, exEx4, exEx5));
		assertEquals(asList("exEx4", "exEx5"),
				result.getOtherSide().stream().map(m -> m.getId()).collect(toList()));
		assertEquals(asList("ex1", "ex2"),
				result.getOwnSide().stream().map(m -> m.getId()).collect(toList()));
		
	}

}
