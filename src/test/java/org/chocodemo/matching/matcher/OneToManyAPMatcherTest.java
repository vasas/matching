package org.chocodemo.matching.matcher;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertEquals;

import org.chocodemo.matching.matchable.Matchable;
import org.junit.Before;
import org.junit.Test;

public class OneToManyAPMatcherTest {

	private OneToManyAPMatcher matcher;

	@Before
	public void setUp() {
		matcher = new OneToManyAPMatcher();
	}

	@Test
	public void test1() {
		Matchable ex = new Matchable("ex1", 9, 4);

		Matchable exEx1 = new Matchable("exEx1", 1, 35);
		Matchable exEx2 = new Matchable("exEx2", 2, 35);
		Matchable exEx3 = new Matchable("exEx3", 3, 3);
		Matchable exEx4 = new Matchable("exEx4", 3, 4);
		Matchable exEx5 = new Matchable("exEx5", 3, 5);

		MatchResult result = matcher.match(ex, singletonList(ex), asList(exEx1, exEx2, exEx3, exEx4, exEx5));
		assertEquals(asList("exEx3", "exEx4", "exEx5"),
				result.getOtherSide().stream().map(m -> m.getId()).collect(toList()));

	}
	
	@Test
	public void test2() {
		Matchable ex = new Matchable("ex1", 13, 5);
		
		Matchable exEx1 = new Matchable("exEx1", 1, 35);
		Matchable exEx2 = new Matchable("exEx2", 2, 35);
		Matchable exEx3 = new Matchable("exEx3", 10, 5);
		Matchable exEx4 = new Matchable("exEx4", 3, 4);
		Matchable exEx5 = new Matchable("exEx5", 3, 5);
		
		MatchResult result = matcher.match(ex, singletonList(ex), asList(exEx1, exEx2, exEx3, exEx4, exEx5));
		assertEquals(asList("exEx3", "exEx5"),
				result.getOtherSide().stream().map(m -> m.getId()).collect(toList()));
		
	}
	
	@Test
	public void test3() {
		Matchable ex = new Matchable("ex1", 100, 5);
		
		Matchable exEx1 = new Matchable("exEx1", 1, 401);
		Matchable exEx2 = new Matchable("exEx2", 2, 35);
		Matchable exEx3 = new Matchable("exEx3", 99, 1);
		Matchable exEx4 = new Matchable("exEx4", 3, 4);
		Matchable exEx5 = new Matchable("exEx5", 3, 15);
		
		MatchResult result = matcher.match(ex, singletonList(ex), asList(exEx1, exEx2, exEx3, exEx4, exEx5));
		assertEquals(asList("exEx1", "exEx3"),
				result.getOtherSide().stream().map(m -> m.getId()).collect(toList()));
		
	}

}
