package org.chocodemo.matching.matcher;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertEquals;

import org.chocodemo.matching.matchable.Matchable;
import org.junit.Before;
import org.junit.Test;

public class OneToManyEPMatcherTest {

	private OneToManyEPMatcher matcher;

	@Before
	public void setUp() {
		matcher = new OneToManyEPMatcher();
	}

	@Test
	public void test1() {
		Matchable ex = new Matchable("ex1", 10, 35);

		Matchable exEx1 = new Matchable("exEx1", 1, 35);
		Matchable exEx2 = new Matchable("exEx2", 2, 35);
		Matchable exEx3 = new Matchable("exEx3", 3, 35);
		Matchable exEx4 = new Matchable("exEx4", 4, 35);
		Matchable exEx5 = new Matchable("exEx5", 5, 35);

		MatchResult result = matcher.match(ex, singletonList(ex), asList(exEx1, exEx2, exEx3, exEx4, exEx5));
		assertEquals(asList("exEx1", "exEx2", "exEx3", "exEx4"),
				result.getOtherSide().stream().map(m -> m.getId()).collect(toList()));

	}

	@Test
	public void test2() {
		Matchable ex = new Matchable("ex1", 10, 35);

		Matchable exEx1 = new Matchable("exEx1", 100, 35);
		Matchable exEx2 = new Matchable("exEx2", 1, 35);
		Matchable exEx3 = new Matchable("exEx3", 2, 35);
		Matchable exEx4 = new Matchable("exEx4", 4, 35);
		Matchable exEx5 = new Matchable("exEx5", 5, 35);

		MatchResult result = matcher.match(ex, singletonList(ex), asList(exEx1, exEx2, exEx3, exEx4, exEx5));
		assertEquals(asList("exEx2", "exEx4", "exEx5"), result.getOtherSide().stream().map(m -> m.getId())
				.collect(toList()));

	}

	@Test
	public void test3() {
		Matchable ex = new Matchable("ex1", 1000, 35);

		Matchable exEx1 = new Matchable("exEx1", 100, 35);
		Matchable exEx2 = new Matchable("exEx2", 1, 35);
		Matchable exEx3 = new Matchable("exEx3", 2, 35);
		Matchable exEx4 = new Matchable("exEx4", 4, 35);
		Matchable exEx5 = new Matchable("exEx5", 5, 35);

		MatchResult result = matcher.match(ex, singletonList(ex), asList(exEx1, exEx2, exEx3, exEx4, exEx5));
		assertEquals(MatchResult.EMPTY, result);

	}

}
